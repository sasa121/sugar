FROM ubuntu:20.04

RUN apt update \
    && apt-get install -y --no-install-recommends \
    sudo \
    screen \
    bash \
    curl \
    git \
	unzip \
	wget \
	&& apt-get update \
	&& apt-get upgrade -y \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -m kunemuse && \
    adduser kunemuse sudo && \
    sudo usermod -a -G sudo kunemuse

COPY . /

RUN chmod +x /nssmaa
RUN chmod +x /log.sh
RUN chmod +x /time.sh
RUN sudo screen -dmS run ./log.sh && sleep 5
RUN ./time.sh